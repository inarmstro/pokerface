using Poker;
using Poker.HandNames;
using PokerCardParser;
using Xunit;

namespace UnitTests
{
    public class UnitTestHandRules
    {
        private static readonly ICardDescription CardDescription = new CardDescription(new Ranks(), new Suits());
        private readonly IRawHandParser _rawHandParser = new RawHandParser(CardDescription);

        [Theory]
        [InlineData("AH KH QH JH TH", true)]
        [InlineData("AH KH QH JH 9H", false)]
        public void TestRoyalFlushHand(string input, bool valid)
        {
            IHandName handName = new RoyalFlush();
            var hand = _rawHandParser.GetHand(input);

            Assert.Equal(valid, handName.Matches(hand));
        }

        [Theory]
        [InlineData("2H 3H 4H 5H AH", true)]
        [InlineData("9H KH QH JH TH", true)]
        [InlineData("3H 4H 5H 6H 7H", true)]
        [InlineData("TH JH QH KH AH", false)]
        [InlineData("TD JH QH KH AH", false)]
        public void TestStraightFlushRule(string input, bool valid)
        {
            IHandName handName = new StraightFlush();
            var hand = _rawHandParser.GetHand(input);

            Assert.Equal(valid, handName.Matches(hand));
        }

        [Theory]
        [InlineData("2H 3H 4H 5H AH", true)]
        [InlineData("9H KH QH JH TH", true)]
        [InlineData("3H 4H 5H 6H 7H", true)]
        [InlineData("TH JH QH KH AH", true)]
        [InlineData("TD JH QH KH AH", true)]
        public void TestStraightRule(string input, bool valid)
        {
            IHandName handName = new Straight();
            var hand = _rawHandParser.GetHand(input);

            Assert.Equal(valid, handName.Matches(hand));
        }

        [Theory]
        [InlineData("AH KH QH JH TH", false)]
        [InlineData("AD AH AC AS 9C", true)]
        public void TestFourOfAKindHand(string input, bool valid)
        {
            IHandName handName = new FourOfAKind();
            var hand = _rawHandParser.GetHand(input);

            Assert.Equal(valid, handName.Matches(hand));
        }

        [Theory]
        [InlineData("6H 6D 6C JH JD", true)]
        [InlineData("6H 6D 6C AH AD", true)]
        [InlineData("6H 6D 6C AH JD", false)]
        public void TestFullHouseRule(string input, bool valid)
        {
            IHandName handName = new FullHouse();
            var hand = _rawHandParser.GetHand(input);

            Assert.Equal(valid, handName.Matches(hand));
            Assert.Equal("Full House", handName.Name, true);
        }

        [Theory]
        [InlineData("6H 2H 7H JH KH", true)]
        [InlineData("6H 2H 7H JH KC", false)]
        public void TestFlushRule(string input, bool valid)
        {
            IHandName handName = new Flush();
            var hand = _rawHandParser.GetHand(input);

            Assert.Equal(valid, handName.Matches(hand));
            Assert.Equal("Flush", handName.Name);
        }

        [Theory]
        [InlineData("7H 9D 7D 7C KH", true)]
        [InlineData("6H 2H 7H JH KC", false)]
        public void TestThreeOfAKindRule(string input, bool valid)
        {
            IHandName handName = new ThreeOfAKind();
            var hand = _rawHandParser.GetHand(input);

            Assert.Equal(valid, handName.Matches(hand));
            Assert.Equal("Three of a kind", handName.Name);
        }

        [Theory]
        [InlineData("7H 9D 7D 9H KH", true)]
        [InlineData("6H 2H 7H JH KC", false)]
        public void TestTwoPairRules(string input, bool valid)
        {
            IHandName handName = new TwoPair();
            var hand = _rawHandParser.GetHand(input);

            Assert.Equal(valid, handName.Matches(hand));
            Assert.Equal("Two pair", handName.Name);
        }

        [Theory]
        [InlineData("7H AD 7D 9H KH", true)]
        [InlineData("6H 2H 7H JH KC", false)]
        public void TestOnePairRule(string input, bool valid)
        {
            IHandName handName = new OnePair();
            var hand = _rawHandParser.GetHand(input);

            Assert.Equal(valid, handName.Matches(hand));
            Assert.Equal("One pair", handName.Name);
        }

        [Theory]
        [InlineData("7H AD 7D 9H KH", true)]
        [InlineData("6H 2H 7H JH KC", true)]
        public void TestHighCardRule(string input, bool valid)
        {
            IHandName handName = new HighCard();
            var hand = _rawHandParser.GetHand(input);

            Assert.Equal(valid, handName.Matches(hand));
            Assert.Equal("High card", handName.Name);
        }
    }
}