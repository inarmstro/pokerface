using System.Linq;
using Poker;
using PokerCardParser;
using Xunit;

namespace UnitTests
{
    public class UnitTestHand
    {
        private readonly ICardDescription _cardDescription;
        private readonly IHand _hand;

        public UnitTestHand()
        {
            _hand = new Hand();
            _cardDescription = new CardDescription(new Ranks(), new Suits());
        }

        [Theory]
        [InlineData("2H", 2, "Hearts")]
        [InlineData("AH", 1, "Hearts")]
        [InlineData("AH", 14, "Hearts")]
        public void TestValidCardDescriptionsReturnICard(string cardDescription, byte containsValue, string suitName)
        {
            var card = _cardDescription.GetCard(cardDescription);
            _hand.AddCard(card);
            _hand.AddCard(card);

            var cards = _hand.GetCards();

            Assert.Equal(1, cards.Count);

            Assert.NotEmpty(cards.Where(x => x.Suit.Name == suitName &&
                                             x.Rank.Values.Contains(containsValue)));
        }
    }
}