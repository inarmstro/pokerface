using System.Collections.Generic;
using Poker;
using Poker.HandNames;
using PokerCardParser;
using Xunit;

namespace UnitTests
{
    public class UnitTestHandIdentifier
    {
        private static readonly IRanks Ranks = new Ranks();
        private static readonly ISuits Suits = new Suits();

        private static readonly List<IHandName> HandNames = new()
        {
            new HighCard(),
            new OnePair(),
            new TwoPair(),
            new ThreeOfAKind(),
            new Straight(),
            new Flush(),
            new FullHouse(),
            new FourOfAKind(),
            new StraightFlush(),
            new RoyalFlush(),
        };


        private static readonly ICardDescription CardDescription = new CardDescription(Ranks, Suits);
        private readonly IHandIdentifier _handIdentifier = new HandIdentifier(HandNames);
        private readonly IRawHandParser _rawHandParser = new RawHandParser(CardDescription);


        [Theory]
        [InlineData("KD 9D 7C 4C 3H", "High card")]
        [InlineData("TC TH 8C 7H 4S", "One pair")]
        [InlineData("JH JC 3S 3C 2H", "Two pair")]
        [InlineData("QC QS QH 9H 2S", "Three of a kind")]
        [InlineData("TD 9S 8H 7D 6C", "Straight")]
        [InlineData("JD 9D 8D 4D 3D", "Flush")]
        [InlineData("6S 6H 6D KC KH", "Full house")]
        [InlineData("5C 5D 5H 5S 2D", "Four of a kind")]
        [InlineData("JC TC 9C 8C 7C", "Straight flush")]
        [InlineData("AC KC QC JC TC", "Royal flush")]
        public void TestIdentify(string rawHand, string name)
        {
            var hand = _rawHandParser.GetHand(rawHand);

            var handName = _handIdentifier.Identify(hand);

            Assert.Equal(name, handName);
        }
    }
}