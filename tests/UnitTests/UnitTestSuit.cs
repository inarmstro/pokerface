using Poker;
using Xunit;

namespace UnitTests
{
    public class UnitTestSuit
    {
        [Theory]
        [InlineData("Hearts", 'H', 'H', true)]
        [InlineData("Hearts", 'H', 'h', false)]
        [InlineData("Diamonds", 'D', 'D', true)]
        [InlineData("Diamonds", 'D', 'a', false)]
        [InlineData("Spades", 'S', 'S', true)]
        [InlineData("Spades", 'S', 's', false)]
        [InlineData("Clubs", 'C', 'C', true)]
        [InlineData("Clubs", 'C', 'c', false)]
        public void TestValidSuits(string name, char suit, char testSuit, bool valid)
        {
            ISuit sut = new Suit(name, suit);

            Assert.Equal(name, sut.Name);
            Assert.Equal(valid, sut.Matches(testSuit));
        }
    }
}