using Poker;
using PokerCardParser;
using Xunit;

namespace UnitTests
{
    public class UnitTestLineParser
    {
        private static readonly ICardDescription CardDescription = new CardDescription(new Ranks(), new Suits());
        private readonly IRawHandParser _rawHandParser = new RawHandParser(CardDescription);


        [Theory]
        [InlineData("2H JS 3C 7C 5D", 5)]
        [InlineData("JH 2C JD 2H 4C", 5)]
        [InlineData("9H 9D 3S 9S 9C", 5)]
        [InlineData("9C 3H 9S 9H 3S", 5)]
        public void TestGetHand(string line, int count)
        {
            var hand = _rawHandParser.GetHand(line);

            Assert.Equal(count, hand.GetCards().Count);
        }
    }
}