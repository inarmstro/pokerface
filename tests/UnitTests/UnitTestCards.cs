using Poker;
using Xunit;

namespace UnitTests
{
    public class UnitTestCards
    {
        private readonly IRanks _ranks = new Ranks();
        private readonly ISuits _suits = new Suits();

        [Theory]
        [InlineData("2H")]
        public void TestCardSuitAndSuit(string cardDescription)
        {
            var suit = _suits.GetSuit(cardDescription[1]);
            var rank = _ranks.GetRank(cardDescription[0]);

            ICard card = new Card(suit, rank);

            Assert.Equal(suit, card.Suit);
            Assert.Equal(rank, card.Rank);
        }
    }
}