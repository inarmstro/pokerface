using Poker;
using PokerCardParser;
using Xunit;

namespace UnitTests
{
    public class UnitTestCardDescription
    {
        private readonly ICardDescription _cardDescription = new CardDescription(new Ranks(), new Suits());

        [Theory]
        [InlineData("2H", 2, "Hearts")]
        [InlineData("AH", 1, "Hearts")]
        [InlineData("AD", 14, "Diamonds")]
        public void TestValidCardDescriptionsReturnICard(string cardDescription, byte containsValue, string suitName)
        {
            var card = _cardDescription.GetCard(cardDescription);

            Assert.Equal(suitName, card.Suit.Name);
            Assert.Contains(containsValue, card.Rank.Values);
        }
    }
}