using Poker;
using Xunit;

namespace UnitTests
{
    public class UnitTestRanks
    {
        private readonly IRanks _ranks = new Ranks();

        [Theory]
        [InlineData('A', "Ace", 1)]
        [InlineData('2', "Two", 2)]
        [InlineData('3', "Three", 3)]
        [InlineData('4', "Four", 4)]
        [InlineData('5', "Five", 5)]
        [InlineData('6', "Six", 6)]
        [InlineData('7', "Seven", 7)]
        [InlineData('8', "Eight", 8)]
        [InlineData('9', "Nine", 9)]
        [InlineData('T', "Ten", 10)]
        [InlineData('J', "Jack", 11)]
        [InlineData('Q', "Queen", 12)]
        [InlineData('K', "King", 13)]
        [InlineData('A', "Ace", 14)]
        public void TestValidRanks(char rankIdentifier, string name, byte containsValue)
        {
            var rank = _ranks.GetRank(rankIdentifier);

            Assert.Equal(rankIdentifier, rank.RankIdentifier);
            Assert.Contains(containsValue, rank.Values);
            Assert.Equal(name, rank.Name);
        }

        [InlineData('P')]
        [Theory]
        public void TestInvalidRankIdentifier_Throws(char rankIdentifier)
        {
            Assert.Throws<RankNotIdentifiedException>(() => _ranks.GetRank(rankIdentifier));
        }
    }
}