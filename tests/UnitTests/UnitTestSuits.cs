using Poker;
using Xunit;

namespace UnitTests
{
    public class UnitTestSuits
    {
        private readonly ISuits _suits = new Suits();

        [Theory]
        [InlineData("Hearts", 'H')]
        [InlineData("Diamonds", 'D')]
        [InlineData("Spades", 'S')]
        [InlineData("Clubs", 'C')]
        public void TestValidSuits(string name, char suitIdentifier)
        {
            Assert.Equal(name, _suits.GetSuit(suitIdentifier).Name);
        }
    }
}