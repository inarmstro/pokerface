using System;
using System.Collections.Generic;
using Nuke.Common;
using Nuke.Common.CI;
using Nuke.Common.Execution;
using Nuke.Common.Git;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.Coverlet;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.GitVersion;
using Nuke.Common.Tools.ReSharper;
using Nuke.Common.Tools.SonarScanner;
using Nuke.Common.Utilities.Collections;
using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.Logger;
using static Nuke.Common.Tools.DotNet.DotNetTasks;
using static Nuke.Common.Tools.ReSharper.ReSharperTasks;
using static Nuke.Common.Tools.SonarScanner.SonarScannerTasks;

[CheckBuildProjectConfigurations]
[ShutdownDotNetAfterServerBuild]
class Build : NukeBuild
{
    const string Debug = "Debug";
    const string Release = "Release";
    const string Net50 = "net5.0";

    [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
    readonly Configuration Configuration = IsLocalBuild ? Configuration.Debug : Configuration.Release;

    readonly int DegreeOfParallelism = Environment.ProcessorCount * 2;

    readonly HashSet<string> ExcludedTests = new();

    [GitRepository] readonly GitRepository GitRepository;

    [GitVersion(Framework = "netcoreapp3.1", NoFetch = true)] readonly GitVersion GitVersion;


    [Solution] readonly Solution Solution;

    [Parameter] readonly string SonarProjectKey;

    [Parameter] readonly string SonarServer = "https://sonarcloud.io";

    [Parameter] readonly string SonarToken;

    [Partition(5)] readonly Partition TestPartition;

    AbsolutePath SourceDirectory => RootDirectory / "src";
    AbsolutePath TestsDirectory => RootDirectory / "tests";
    AbsolutePath SolutionFile => SourceDirectory / "PokerFace.sln";
    AbsolutePath OutputDirectory => RootDirectory / "output";
    AbsolutePath TestResultDirectory => OutputDirectory / "test-results";
    AbsolutePath CoverageReportDirectory => OutputDirectory / "coverage-reports";
    AbsolutePath PackageDirectory => OutputDirectory / "packages";

    IEnumerable<Project> TestProjects => TestPartition.GetCurrent(
        Solution.GetProjects("UnitTests"));

    Target Clean => _ => _
        .Before(Restore)
        .Executes(() =>
        {
            SourceDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
            TestsDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
            EnsureCleanDirectory(OutputDirectory);
        });

    Target Restore => _ => _
        .Executes(() =>
        {
            DotNetRestore(s => s
                .SetProjectFile(Solution));
        });

    Target Compile => _ => _
        .DependsOn(Restore)
        .Executes(() =>
        {
            DotNetBuild(s => s
                .SetProjectFile(Solution)
                .SetConfiguration(Configuration)
                .EnableNoLogo()
                .SetAssemblyVersion(GitVersion.AssemblySemVer)
                .SetFileVersion(GitVersion.AssemblySemFileVer)
                .SetInformationalVersion(GitVersion.InformationalVersion)
                .EnableNoRestore());
        });

    Target Test => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            DotNetTest(TestSettings);
        });

    Target TestWithCoverage => _ => _
        .DependsOn(Compile)
        .Produces(TestResultDirectory / "*.trx")
        .Produces(TestResultDirectory / "*.xml")
        .Partition(() => TestPartition)
        .Executes(() =>
        {
            DotNetTest(
                CoverSettings,
                DegreeOfParallelism,
                true);
        });

    Target Analysis => _ => _
        .Before(Compile)
        .Executes(() =>
        {
            ReSharperInspectCode(s => s
                .SetTargetPath(Solution)
                .SetOutput(OutputDirectory / "Resharper.xml")
                .DisableNoSwea());
        });

    Target AutoRefactor => _ => _
        .Executes(() =>
        {
            ReSharperCleanupCode(s => s
                .SetTargetPath(Solution)
            );
        });

    Target Sonar => _ => _
        .DependsOn(TestWithCoverage)
        .Consumes(TestWithCoverage)
        .Executes(() =>
        {
            Info("Creating Sonar analysis for version: {Version} ...", GitVersion.SemVer);
            SonarScannerBegin(SonarBeginFullSettings);

            DotNetBuild(SonarBuildAll);

            SonarScannerEnd(SonarEndSettings);
        });

    /// Support plugins are available for:
    /// - JetBrains ReSharper        https://nuke.build/resharper
    /// - JetBrains Rider            https://nuke.build/rider
    /// - Microsoft VisualStudio     https://nuke.build/visualstudio
    /// - Microsoft VSCode           https://nuke.build/vscode
    public static int Main() => Execute<Build>(x => x.Compile);

    IEnumerable<DotNetTestSettings> TestSettings(DotNetTestSettings settings) =>
        TestBaseSettings(settings)
            .CombineWith(TestProjects, (_, v) => _
                .SetProjectFile(v)
                .EnableNoRestore()
                .EnableNoBuild());

    SonarScannerBeginSettings SonarBeginFullSettings(SonarScannerBeginSettings settings) =>
        SonarBeginBaseSettings(settings)
            .SetVersion(GitVersion.SemVer)
            .SetFramework(Net50);

    SonarScannerBeginSettings SonarBeginBaseSettings(SonarScannerBeginSettings settings) =>
        SonarBaseSettings(settings)
            .SetProcessWorkingDirectory(RootDirectory)
            .SetProjectKey(SonarProjectKey)
            .SetName("PokerFace")
            .SetServer(SonarServer)
            .SetLogin(SonarToken)
            .AddOpenCoverPaths(TestResultDirectory / "*.xml")
            .SetVSTestReports(TestResultDirectory / "*.trx")
            // .AddSourceExclusions("**/Generated/**/*.*,**/*.Designer.cs,**/*.generated.cs,**/*.js,**/*.html,**/*.css,**/Sample/**/*.*,**/Samples.*/**/*.*,**/*Tools.*/**/*.*,**/Program.Dev.cs, **/Program.cs,**/*.ts,**/*.tsx,**/*EventSource.cs,**/*EventSources.cs,**/*.Samples.cs,**/*Tests.*/**/*.*,**/*Test.*/**/*.*")
            .SetProcessArgumentConfigurator(t => t
                .Add("/o:{0}", "inarmstro")
                .Add("/d:sonar.cs.roslyn.ignoreIssues={0}", "true")
            );

    SonarScannerBeginSettings SonarBaseSettings(SonarScannerBeginSettings settings) =>
        settings
            .SetLogin(SonarToken)
            .SetProcessWorkingDirectory(RootDirectory);

    SonarScannerEndSettings SonarEndSettings(SonarScannerEndSettings settings) =>
        settings
            .SetLogin(SonarToken)
            .SetProcessWorkingDirectory(RootDirectory)
            .SetFramework(Net50);

    DotNetBuildSettings SonarBuildAll(DotNetBuildSettings settings) =>
        settings
            .SetProjectFile(Solution)
            .EnableNoRestore()
            .SetConfiguration(Debug)
            .SetProcessWorkingDirectory(RootDirectory);

    IEnumerable<DotNetTestSettings> CoverSettings(DotNetTestSettings settings) =>
        TestBaseSettings(settings)
            .EnableCollectCoverage()
            .SetProcessArgumentConfigurator(a => a
                .Add("--collect:\"XPlat Code Coverage\"")
            )
            .SetCoverletOutputFormat(CoverletOutputFormat.opencover)
            .SetExcludeByFile("*.Generated.cs")
            .CombineWith(TestProjects, (_, v) => _
                .SetProjectFile(v)
                .SetLogger($"trx;LogFileName={v.Name}.trx")
                .SetCoverletOutput(TestResultDirectory / $"{v.Name}.xml")
            );

    DotNetTestSettings TestBaseSettings(DotNetTestSettings settings) =>
        settings
            .SetConfiguration(Debug)
            .EnableNoRestore()
            .EnableNoBuild()
            .ResetVerbosity()
            .SetResultsDirectory(TestResultDirectory);
}