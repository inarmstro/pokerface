using System.Text.RegularExpressions;
using Poker;

namespace PokerCardParser
{
    public class RawHandParser : IRawHandParser
    {
        private readonly ICardDescription _cardDescription;
        private readonly Regex _regex = new(@"^([2-9TJQKA][HDCS][ ]{0,}){5}$");

        public RawHandParser(ICardDescription cardDescription)
        {
            _cardDescription = cardDescription;
        }

        public IHand GetHand(string rawHand)
        {
            IHand hand = new Hand();

            if (!_regex.IsMatch(rawHand))
            {
                return hand;
            }

            foreach (var cardDescriptor in _cardDescription.GetCardDescriptors(rawHand))
            {
                hand.AddCard(_cardDescription.GetCard(cardDescriptor));
            }

            return hand;
        }
    }
}