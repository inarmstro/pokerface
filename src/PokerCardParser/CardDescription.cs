using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Poker;

namespace PokerCardParser
{
    public class CardDescription : ICardDescription
    {
        private readonly Regex _cardDescriptor = new(@"([2-9TJQKA][HDCS])");
        private readonly IRanks _ranks;
        private readonly Regex _regex = new(@"(?<Rank>[2-9,T,J,Q,K,A])(?<Suit>[H,D,C,S])");
        private readonly ISuits _suits;

        public CardDescription(
            IRanks ranks,
            ISuits suits
        )
        {
            _ranks = ranks;
            _suits = suits;
        }

        public IEnumerable<string?> GetCardDescriptors(string rawHand)
        {
            foreach (Match? m in _cardDescriptor.Matches(rawHand))
            {
                yield return m?.Value;
            }
        }

        public ICard GetCard(string? cardDescription)
        {
            if (string.IsNullOrEmpty(cardDescription))
            {
                throw new ArgumentException("{Name} is null or empty", nameof( cardDescription ));
            }
            var matches = _regex.Match(cardDescription);

            var rank = _ranks.GetRank(matches.Groups["Rank"].Value[0]);
            var suit = _suits.GetSuit(matches.Groups["Suit"].Value[0]);

            return new Card(suit, rank);
        }
    }
}