using System.Collections.Generic;
using Poker;

namespace PokerCardParser
{
    public interface ICardDescription
    {
        IEnumerable<string?> GetCardDescriptors(string rawHand);

        ICard GetCard(string? cardDescription);
    }
}