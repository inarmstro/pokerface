using Poker;

namespace PokerCardParser
{
    public interface IRawHandParser
    {
        IHand GetHand(string rawHand);
    }
}