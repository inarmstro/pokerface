namespace Poker
{
    public class Suit : ISuit
    {
        private readonly char _suitIdentifier;

        public Suit(string name, char suitIdentifier)
        {
            Name = name;
            _suitIdentifier = suitIdentifier;
        }

        public string Name { get; }

        public bool Matches(char suitIdentifier)
        {
            return suitIdentifier == _suitIdentifier;
        }
    }
}