using System;

namespace Poker
{
    public class RankNotIdentifiedException : Exception
    {
        public RankNotIdentifiedException(string s) : base(s)
        {
        }
    }
}