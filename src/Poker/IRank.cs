using System.Collections.Generic;

namespace Poker
{
    public interface IRank
    {
        char RankIdentifier { get; }
        IList<ushort> Values { get; }
        string Name { get; }
        bool Matches(char rankIdentifier);
    }
}