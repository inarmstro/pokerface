using System.Collections.Generic;
using System.Linq;

namespace Poker
{
    public class Suits : ISuits
    {
        private readonly List<ISuit> _suits;

        public Suits()
        {
            _suits = new List<ISuit>
            {
                new Suit("Hearts", 'H'),
                new Suit("Diamonds", 'D'),
                new Suit("Clubs", 'C'),
                new Suit("Spades", 'S'),
            };
        }

        public ISuit GetSuit(char suitIdentifier)
        {
            return _suits.FirstOrDefault(x => x.Matches(suitIdentifier));
        }
    }
}