using System.Collections.Generic;

namespace Poker
{
    public interface IHand
    {
        IList<ICard> GetCards();

        void AddCard(ICard card);
    }
}