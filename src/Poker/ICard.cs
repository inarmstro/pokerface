namespace Poker
{
    public interface ICard
    {
        ISuit Suit { get; }
        IRank Rank { get; }
    }
}