using System.Collections.Generic;
using System.Linq;
using Poker.HandNames;

namespace Poker
{
    public class HandIdentifier : IHandIdentifier
    {
        private readonly IList<IHandName> _handNames;

        public HandIdentifier(IList<IHandName> handNames)
        {
            _handNames = handNames;
        }

        public string Identify(IHand hand)
        {
            // Search the available hand for a match
            // Order the possible matches by rank
            // and return the name of the first match
            return _handNames.Where(x => x.Matches(hand))
                .OrderBy(x => x.Rank)
                .First()
                .Name;
        }
    }
}