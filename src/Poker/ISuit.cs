namespace Poker
{
    public interface ISuit
    {
        string Name { get; }
        bool Matches(char suitIdentifier);
    }
}