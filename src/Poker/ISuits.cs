namespace Poker
{
    public interface ISuits
    {
        ISuit GetSuit(char suitIdentifier);
    }
}