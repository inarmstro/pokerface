namespace Poker
{
    public interface IRanks
    {
        IRank GetRank(char rankIdentifier);
    }
}