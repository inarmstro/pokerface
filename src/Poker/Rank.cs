using System.Collections.Generic;

namespace Poker
{
    public class Rank : IRank
    {
        public Rank(char rankIdentifier, string name, byte value)
        {
            RankIdentifier = rankIdentifier;
            Name = name;
            Values = new List<ushort>();
            AddValue(value);
        }

        public Rank(char rankIdentifier, string name, IList<ushort> values)
        {
            RankIdentifier = rankIdentifier;
            Name = name;
            Values = values;

            foreach (var v in values)
            {
                AddValue(v);
            }
        }

        public char RankIdentifier { get; }
        public IList<ushort> Values { get; }

        public bool Matches(char rankIdentifier)
        {
            return RankIdentifier == rankIdentifier;
        }

        public string Name { get; }

        private void AddValue(ushort value)
        {
            if (!Values.Contains(value))
            {
                Values.Add(value);
            }
        }
    }
}