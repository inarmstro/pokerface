using System.Collections.Generic;
using System.Linq;

namespace Poker
{
    public class Hand : IHand
    {
        private readonly IList<ICard> _cards;

        public Hand()
        {
            _cards = new List<ICard>();
        }

        public IList<ICard> GetCards()
        {
            return _cards;
        }

        public void AddCard(ICard card)
        {
            if (_cards.FirstOrDefault(x =>
                x.Rank == card.Rank &&
                x.Suit == card.Suit) == null)
            {
                _cards.Add(card);
            }
        }
    }
}