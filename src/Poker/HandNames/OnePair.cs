using System.Linq;

namespace Poker.HandNames
{
    /// <summary>
    ///     One pair is a hand that contains two cards of one rank and
    ///     three cards of three other ranks.
    /// </summary>
    public class OnePair : IHandName
    {
        public OnePair()
        {
            Name = "One pair";
            Rank = 10;
        }

        public bool Matches(IHand hand)
        {
            // There will be 4 distinct rank name i.e. 2 x ten, and 3 of any other rank. 
            return hand.GetCards()
                .Select(x => x.Rank.Name)
                .Distinct()
                .Count() == 4;
        }

        public string Name { get; }
        public uint Rank { get; }
    }
}