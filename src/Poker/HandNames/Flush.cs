using System.Linq;

namespace Poker.HandNames
{
    /// <summary>
    ///     A flush is a hand that contains five cards all of the same suit,
    ///     not all of sequential rank.
    /// </summary>
    public class Flush : IHandName
    {
        public Flush()
        {
            Name = "Flush";
            Rank = 6;
        }

        public bool Matches(IHand hand)
        {
            return hand.GetCards()
                .Select(x => x.Suit)
                .GroupBy(x => x.Name)
                .Any(r => r.Count() == 5);
        }

        public string Name { get; }
        public uint Rank { get; }
    }
}