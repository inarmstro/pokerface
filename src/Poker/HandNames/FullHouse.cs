using System.Linq;

namespace Poker.HandNames
{
    /// <summary>
    ///     A full house is a hand that contains three cards of one rank
    ///     and two cards of another rank
    /// </summary>
    public class FullHouse : IHandName
    {
        public FullHouse()
        {
            Name = "Full house";
            Rank = 5;
        }

        public bool Matches(IHand hand)
        {
            return hand.GetCards()
                .Select(x => x.Rank)
                .GroupBy(x => x.Values)
                .All(r => r.Count() == 3 || r.Count() == 2);
        }

        public string Name { get; }
        public uint Rank { get; }
    }
}