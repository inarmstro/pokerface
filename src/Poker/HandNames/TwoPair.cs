using System.Linq;

namespace Poker.HandNames
{
    /// <summary>
    ///     Two pair is a hand that contains two cards of one rank,
    ///     two cards of another rank and one card of a third rank.
    /// </summary>
    public class TwoPair : IHandName
    {
        public TwoPair()
        {
            Name = "Two pair";
            Rank = 9;
        }

        public bool Matches(IHand hand)
        {
            // There will be 3 distinct rank names i.e. 2 x ten, 2 x ace and 1 other rank.
            return hand.GetCards()
                .Select(x => x.Rank.Name)
                .Distinct()
                .Count() == 3;
        }

        public string Name { get; }
        public uint Rank { get; }
    }
}