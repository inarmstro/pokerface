using System.Linq;

namespace Poker.HandNames
{
    /// <summary>
    ///     Three of a kind is a hand that contains three cards of one rank and
    ///     two cards of two other ranks
    /// </summary>
    public class ThreeOfAKind : IHandName
    {
        public ThreeOfAKind()
        {
            Name = "Three of a kind";
            Rank = 8;
        }

        public bool Matches(IHand hand)
        {
            // Check for 3 cards of the same rank.
            return hand.GetCards()
                .Select(x => x.Rank)
                .GroupBy(x => x.Name)
                .Any(r => r.Count() == 3);
        }

        public string Name { get; }
        public uint Rank { get; }
    }
}