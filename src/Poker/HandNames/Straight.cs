using System;
using System.Linq;

namespace Poker.HandNames
{
    /// <summary>
    ///     A straight is a hand that contains five cards of sequential rank,
    ///     not all of the same suit.
    /// </summary>
    public class Straight : IHandName
    {
        public Straight()
        {
            Name = "Straight";
            Rank = 7;
        }

        public bool Matches(IHand hand)
        {
            var values = hand.GetCards()
                .Select(x => x.Rank)
                .SelectMany(r => r.Values)
                .OrderBy(x => x)
                .ToList();

            // If there is any ace check both top and bottom of the range.
            if (values.Count == 6)
            {
                return Math.Abs(values[4] - values[0]) == 4 ||
                       Math.Abs(values[5] - values[1]) == 4;
            }

            return Math.Abs(values[4] - values[0]) == 4;
        }

        public string Name { get; }
        public uint Rank { get; }
    }
}