using System.Linq;

namespace Poker.HandNames
{
    /// <summary>
    ///     A straight flush is a hand that contains five cards of sequential rank,
    ///     all of the same suit.
    /// </summary>
    public class StraightFlush : IHandName
    {
        public StraightFlush()
        {
            Name = "Straight flush";
            Rank = 3;
        }

        public bool Matches(IHand hand)
        {
            var cards = hand.GetCards();

            // Check all the cards are from the same suit.
            var s = cards
                .Select(x => x.Suit)
                .GroupBy(x => x.Name)
                .All(x => x.Count() == 5);

            if (!s)
            {
                return false;
            }

            // Get all the value as a list.
            var values = cards.Select(x => x.Rank.Values)
                .SelectMany(v => v)
                .ToList();

            values.Sort();

            // Check that the 5th element of greater than the 1 by 4.
            return values[4] - values[0] == 4;
        }


        public string Name { get; }
        public uint Rank { get; }
    }
}