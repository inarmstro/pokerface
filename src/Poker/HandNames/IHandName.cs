namespace Poker.HandNames
{
    public interface IHandName
    {
        string Name { get; }
        uint Rank { get; }
        bool Matches(IHand hand);
    }
}