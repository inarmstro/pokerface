namespace Poker.HandNames
{
    /// <summary>
    ///     High card is a hand that does not fall into any other category.
    /// </summary>
    public class HighCard : IHandName
    {
        public HighCard()
        {
            Name = "High card";
            Rank = 11;
        }

        public bool Matches(IHand hand)
        {
            return hand.GetCards().Count == 5;
        }

        public string Name { get; }
        public uint Rank { get; }
    }
}