using System.Linq;

namespace Poker.HandNames
{
    /// <summary>
    ///     A straight is a hand that contains five cards of sequential rank,
    ///     not all of the same suit.
    /// </summary>
    public class FourOfAKind : IHandName
    {
        public FourOfAKind()
        {
            Name = "Four of a kind";
            Rank = 4;
        }

        public bool Matches(IHand hand)
        {
            return hand.GetCards()
                .Select(x => x.Rank)
                .GroupBy(x => x.Name)
                .Any(x => x.Count() == 4);
        }

        public string Name { get; }
        public uint Rank { get; }
    }
}