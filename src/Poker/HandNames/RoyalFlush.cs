using System.Linq;

namespace Poker.HandNames
{
    /// <summary>
    ///     A royal flush is a hand that contains five cards of sequential rank,
    ///     all of the same suit, starting at ten, joker, queen, king and ace.
    /// </summary>
    public class RoyalFlush : IHandName
    {
        public RoyalFlush()
        {
            Name = "Royal flush";
            Rank = 1;
        }

        public bool Matches(IHand hand)
        {
            var cards = hand.GetCards();

            var s = cards
                .Select(x => x.Suit)
                .GroupBy(x => x.Name)
                .All(x => x.Count() == 5);

            if (!s)
            {
                return false;
            }

            var values = cards
                .Select(x => x.Rank.Values)
                .SelectMany(v => v)
                .OrderBy(x => x)
                .ToList();

            // Check there are 6 value i.e. contains an ace
            // The 2nd element must contain 10.
            return values.Count == 6 &&
                   values[1] == 10;
        }

        public string Name { get; }
        public uint Rank { get; }
    }
}