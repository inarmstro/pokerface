namespace Poker
{
    public class Card : ICard
    {
        public Card(ISuit suit, IRank rank)
        {
            Suit = suit;
            Rank = rank;
        }

        public ISuit Suit { get; }
        public IRank Rank { get; }
    }
}