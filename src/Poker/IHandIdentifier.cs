namespace Poker
{
    public interface IHandIdentifier
    {
        string Identify(IHand hand);
    }
}