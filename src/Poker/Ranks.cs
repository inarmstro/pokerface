using System.Collections.Generic;
using System.Linq;

namespace Poker
{
    public class Ranks : IRanks
    {
        private readonly IList<IRank> _ranks;

        public Ranks()
        {
            _ranks = new List<IRank>
            {
                new Rank('2', "Two", 2),
                new Rank('3', "Three", 3),
                new Rank('4', "Four", 4),
                new Rank('5', "Five", 5),
                new Rank('6', "Six", 6),
                new Rank('7', "Seven", 7),
                new Rank('8', "Eight", 8),
                new Rank('9', "Nine", 9),
                new Rank('T', "Ten", 10),
                new Rank('J', "Jack", 11),
                new Rank('Q', "Queen", 12),
                new Rank('K', "King", 13),
                new Rank('A', "Ace", new List<ushort> {1, 14}),
            };
        }

        public IRank GetRank(char rankIdentifier)
        {
            var rank = _ranks.FirstOrDefault(x => x.Matches(rankIdentifier));

            if (rank == null)
            {
                throw new RankNotIdentifiedException($"Failed to identify a rank for '{rankIdentifier}'.");
            }

            return rank;
        }
    }
}